# DJANGO QUIZ :mortar_board:

> Ce projet est une version fonctionnelle actualisée de Tom Walker (https://github.com/tomwalker/django_quiz).

Il propose la possibilité de créer des quizs de type QCM ou de type ESSAY, l'administrateur peux accéder à tous les résultats, les utilisateurs peuvent consulter leurs scores ainsi que leur statistiques.


L'administration est gérée par Django admin et permet de créer des utilisateurs facilement.
Pour l'utilisation d'OpenID (avec Keycloak), le login "traditionnel" est désactivé mais une fois les réglages OIDC éffectués, la création d'utilisateur s'éffectue par keycloak.

Les modifications apportées concernent la librairy pour la lectures des classes codées en python2 :
- Utilisation de la librairy six et utilisation de Django 2.2.9 (la 3.0 ne prends plus en charge python 2).
- Modification du requirements.txt en conséquence.
- Implémentation d'OpenID connect avec Keycloak settings via mozilla oidc module (dans la branche keycloak).

## PRE REQUIS :paperclip:

- python 3.6 au minimum (fonctionne sous 3.11)
- pip3
- Docker et compose

## INSTALLATION LOCALE :house:

```bash
git clone https://git.legaragenumerique.fr/GARAGENUM/django-quiz.git
cd django-quiz/quiz-app
python3 manage.py runserver
```

### CEER UN UTILISATEUR ADMIN :bust_in_silhouette:

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser # suivre les indications pour créer un utilisateur admin
```

> Se rendre à l'adresse http://localhost:8000/admin pour se connecter

## VIA DOCKER :whale:

```bash
git clone https://git.legaragenumerique.fr/GARAGENUM/django-quiz.git
cd django-quiz
docker compose up -d
```

### CEER UN UTILISATEUR ADMIN :bust_in_silhouette:

```bash
# pour se connecter au terminal du conteneur
docker exec -it django-quiz bash
# dans le terminal du conteneur
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser # suivre les indications pour créer un utilisateur admin
```

> Se rendre à l'adresse http://localhost:8000/admin pour se connecter

## PRE REQUIS :paperclip:

- python 3.6 au minimum (fonctionne sous 3.11)
- pip3
- Docker et compose

## CONFIGURATION

- testenv_django_quiz/settings.py:

```python
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'put-a-good-secret-key!'

# security token
CSRF_TRUSTED_ORIGINS = ['https://quiz.your-domain/tld']
```

## INSTALLATION LOCALE :house:

```bash
git clone https://git.legaragenumerique.fr/GARAGENUM/django-quiz/src/branch/keycloak.git
cd django-quiz/quiz-app
python3 -m pip install -r requirements.txt
python3 manage.py runserver
```

### CEER UN UTILISATEUR ADMIN :bust_in_silhouette:

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser # suivre les indications pour créer un utilisateur admin
```

> Se rendre à l'adresse http://localhost:8000/admin pour se connecter

## VIA DOCKER :whale:

```bash
git clone https://git.legaragenumerique.fr/GARAGENUM/django-quiz/src/branch/keycloak.git
cd django-quiz
docker compose up -d
```

### CEER UN UTILISATEUR ADMIN :bust_in_silhouette:

```bash
# pour se connecter au terminal du conteneur
docker exec -it django-quiz bash
# dans le terminal du conteneur
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser # suivre les indications pour créer un utilisateur admin
```

> Se rendre à l'adresse http://localhost:8000/admin pour se connecter

## TODO :bookmark_tabs:

- [x] Modify user creation and update methods
- [x] Docker-compose.yml
- [x] OpenID with keycloak
- [ ] Secure secrets in env + ports
- [ ] Pass groups with user creation from Keycloak
- [ ] Automatiser creation superuser at startup

## VERSION AUTHENTIFICATION EXTERNE (SSO KEYCLOAK) :key:

> https://git.legaragenumerique.fr/GARAGENUM/django-quiz/src/branch/keycloak

## TODO :bookmark_tabs:

- [ ] filtre Quiz par catégories
- [ ] filtre questions par quizs
