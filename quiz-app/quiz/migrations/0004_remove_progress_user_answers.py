# Generated by Django 3.2 on 2021-05-11 14:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0003_progress_user_answers'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='progress',
            name='user_answers',
        ),
    ]
