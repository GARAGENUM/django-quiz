��          �      L      �  N   �          ,     ?     G  
   `     k     �     �     �     �     �     �       h   )     �      �  "   �  A  �  U   7     �     �     �  *   �        1        B     W     f  +   }  '   �  "   �     �  �     )   �  .   �  $   �                                                                         	   
                        An email with the reset password instructions has been sent to the given email Click here to connect again Confirm password : Connect Connect to see this page Disconnect Enter the recovery email : Forgotten password Logged out ! New password : Password reset failed : Please enter your new password Please follow the link below : The password has been changed The reset password link wasn't valid. Maybe it has been already used. Please ask to reset password again Wrong username or/and password You asked to reset your password You don't have access to this page Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Un email avec les instructions pour réinitialiser votre mot de passe a été envoyé Clique ici pour te reconnecter Confirmez le mot de passe : Se connecter Connectez-vous pour accéder à cette page Se déconnecter Veuillez entrer l'adresse email de récupération Mot de passe oublié Déconnecté ! Nouveau mot de passe : Réinitialisation du mot de passe échouée Veuillez entrez le nouveau mot de passe Veuillez suivre le lien ci-dessous Le mot de passe a été changé Le lien de réinitialisation du mot de passe est invalide. Il  a peux être été déjà utilisé. Veuillez réitérer votre requête Combinaison utilisateur/mot de passe faux Vous avez demandé à modifier le mot de passe Vous n'avez pas accès à cette page 