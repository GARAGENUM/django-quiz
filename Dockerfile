FROM python:3.11-slim-bookworm

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y \
    gcc \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /code
WORKDIR /code

COPY quiz-app/requirements.txt /code/
RUN pip install -r requirements.txt
COPY quiz-app/. /code/
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]

# BUILD
FROM python:3.11-slim-bookworm AS builder

WORKDIR /app

RUN apt-get update && apt-get install -y \
    gcc \
    && rm -rf /var/lib/apt/lists/*

COPY quiz-app/requirements.txt .
RUN pip install --no-cache-dir --prefix=/install -r requirements.txt

# FINAL
FROM python:3.11-slim-bookworm

WORKDIR /code

COPY --from=builder /install /usr/local
COPY quiz-app/. .

EXPOSE 8000
CMD ["gunicorn", "-b", "0.0.0.0:8000", "app.wsgi:application"]
